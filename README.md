# Docker Debian Buster - rbenv - Rails - General

This repository is used for building a custom Docker image for [the new Ruby.MN site](https://github.com/jhsu802701/rubymn2).

## Name of This Docker Image
[registry.gitlab.com/jhsu802701/docker-debian-buster-rbenv-rails-rubymn2](https://gitlab.com/jhsu802701/docker-debian-buster-rbenv-rails-rubymn2/container_registry)

## Upstream Docker Image
[registry.gitlab.com/rubyonracetracks/docker-debian-buster-min-rbenv](https://gitlab.com/rubyonracetracks/docker-debian-buster-min-rbenv/container_registry)

## What's Added
* The latest versions of the rails, pg, nokogiri, and ffi gems
* The versions of the above gems used in the new Ruby.MN app
* The mailcatcher gem
* The correct Ruby version WITH the above gems plus the Ruby version to upgrade to

## More Information
General information common to all Docker Debian build repositories is in the [FAQ](https://gitlab.com/rubyonracetracks/docker-debian-common/blob/master/FAQ.md).
